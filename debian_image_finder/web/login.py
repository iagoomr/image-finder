import os
from flask import redirect, url_for, flash
from flask_login import login_user
from flask_dance.contrib.gitlab import gitlab
from flask_dance.consumer import oauth_authorized
from debian_image_finder.models.user import User
from debian_image_finder.extensions.sso import blueprint

os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'


class SSOError(Exception):
    pass


@oauth_authorized.connect_via(blueprint)
def logged_in(blueprint, token):

    account_info = blueprint.session.get('user')

    if account_info.ok:
        account_info_json = account_info.json()
        name = account_info_json['name']
        username = account_info_json['username']
        email = account_info_json['email']
        avatar_url = account_info_json['avatar_url']

        user = User.find_by_username(username)

        if user:
            user.name = name
            user.email = email
            user.avatar_url = avatar_url
            user.save_to_db()
        else:
            user = User(name=name,
                        username=username,
                        email=email,
                        avatar_url=avatar_url,
                        admin=False)
            user.save_to_db()

        login_user(user)
        flash('You were successfully logged in.', category='success')


def login():
    if not gitlab.authorized:
        return redirect(url_for("gitlab.login"))
