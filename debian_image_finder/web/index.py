from flask import render_template
from debian_image_finder.models.provider import Provider


def index():
    providers = Provider.find_all()
    return render_template('home.html',
                           providers=providers)
