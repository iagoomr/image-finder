import datetime
from typing import List
from debian_image_finder.extensions.database import db


class Package(db.Model):
    __tablename__ = 'packages'

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    version = db.Column(db.String(80), nullable=False)
    tracker = db.Column(db.String(150))
    created_at = db.Column(db.DateTime(), nullable=False)

    def __init__(self, name, version, tracker):
        self.name = name
        self.version = version
        self.tracker = tracker
        self.created_at = datetime.datetime.utcnow()

    @classmethod
    def find_by_id(cls, _id) -> 'Package':
        return cls.query.filter_by(id=_id).first_or_404()

    @classmethod
    def find_by_name(cls, name) -> 'Package':
        return cls.query.filter_by(name=name).first_or_404()

    @classmethod
    def find_by_name_and_version(cls, name, version) -> 'Package':
        return cls.query.filter_by(name=name, version=version).first()

    @classmethod
    def find_by_version(cls, version) -> 'Package':
        return cls.query.filter_by(version=version).first_or_404()

    @classmethod
    def find_all(cls) -> List['Package']:
        return cls.query.all()

    def update(self, data) -> 'Package':
        self.name = data['name']
        self.version = data['version']
        self.tracker = data['tracker']
        return self

    def save_to_db(self) -> None:
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self) -> None:
        db.session.delete(self)
        db.session.commit()

    def __repr__(self):
        return 'Package(name=%s, version=%s, tracker=%s)' % (
            self.name, self.version, self.tracker)
