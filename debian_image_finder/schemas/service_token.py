from debian_image_finder.extensions.serialization import ma
from debian_image_finder.models.service_token import ServiceToken


class ServiceTokenSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ServiceToken
        load_instance = True


service_token_schema = ServiceTokenSchema()
service_tokens_schema = ServiceTokenSchema(many=True)
