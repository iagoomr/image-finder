from debian_image_finder.extensions.serialization import ma
from marshmallow import validates, validates_schema, ValidationError
from debian_image_finder.models.provider import Provider


class ProviderSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Provider
        load_instance = True

    @validates('id')
    def validate_id(self, value):
        raise ValidationError('Please do not send the id parameter.')

    @validates('created_at')
    def validate_created_at(self, value):
        raise ValidationError('Please do not send the created_at parameter.')

    @validates_schema
    def validate_vendor(self, data, **kwargs):
        provider = Provider.find_by_vendor(data['vendor'])
        if provider:
            raise ValidationError('Provider vendor already exists.')

    @validates_schema
    def validate_name(self, data, **kwargs):
        provider = Provider.find_by_name(data['name'])
        if provider:
            raise ValidationError('Provider name already exists.')


provider_schema = ProviderSchema(exclude=("id", "created_at",))
providers_schema = ProviderSchema(exclude=("id", "created_at",), many=True)
