from flask_migrate import Migrate
from debian_image_finder.extensions.database import db

migrate = Migrate()


def init_app(app):
    db.app = app
    if db.engine.url.drivername == 'sqlite':
        migrate.init_app(app, db, render_as_batch=True)
    else:
        migrate.init_app(app, db)
