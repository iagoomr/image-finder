"""Add public_id to service_tokens

Revision ID: 0159961e4c95
Revises: c5b70f52335a
Create Date: 2022-02-15 00:54:58.025107

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0159961e4c95'
down_revision = 'c5b70f52335a'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('service_tokens', schema=None) as batch_op:
        batch_op.add_column(
            sa.Column('public_id', sa.String(length=255), nullable=True)
        )
        batch_op.create_unique_constraint(None, ['public_id'])


def downgrade():
    with op.batch_alter_table('service_tokens', schema=None) as batch_op:
        batch_op.drop_constraint(None, type_='unique')
        batch_op.drop_column('public_id')
